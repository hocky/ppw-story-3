# PPW Story

This is a static HTML branch of [this repo](https://gitlab.com/hocky/ppw-story).

The website is based on this [wireframe](https://wireframe.cc/LPtOul) and [prototype](https://www.figma.com/file/H1o2XZKeX7sXv2I0qpxx9F/Story-2-Hocky?node-id=4%3A2).

[![pipeline status](https://gitlab.com/hocky/ppw-story/badges/master/pipeline.svg)](https://gitlab.com/hocky/ppw-story/-/commits/master) [![coverage report](https://gitlab.com/hocky/ppw-story/badges/master/coverage.svg)](https://gitlab.com/hocky/ppw-story/-/commits/master)

This has been deployed and available [here](http://hockyy.com/).

|       |                                |
|-------|--------------------------------|
| Nama  | Hocky Yudhiono                 |
| NPM   | 1906285604                     |
| Kelas | PPW-B                          |
| Asdos | Muhammad Ariq Basyar (AB)      |